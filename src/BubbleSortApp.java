public class BubbleSortApp {
    public static void main(String[] args) {
        int[] table = {5, 3, 8, 1, 2, 2, 4, 1};
        print(sort(table));
    }

    public static int[] sort(int[] tabToSort) {
        for (int i = 0; i < tabToSort.length - 1; i++) {
            for (int j = 0; j < tabToSort.length - 1; j++) {
                if (tabToSort[j] > +tabToSort[j + 1]) {
                    int tmp = tabToSort[j + 1];
                    tabToSort[j + 1] = tabToSort[j];
                    tabToSort[j] = tmp;
                }
            }
        }
        return tabToSort;
    }

    public static void print(int[] tabToPrint) {
        for (int j : tabToPrint) {
            System.out.println(j);
        }
    }
}
